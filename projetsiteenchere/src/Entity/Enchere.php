<?php

namespace App\Entity;

use App\Repository\EnchereRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EnchereRepository::class)
 */
class Enchere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixImmediat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixDepart;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bid;

    /**
     * @ORM\ManyToOne(targetEntity=Annonce::class, inversedBy="encheres")
     */
    private $annonce;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datefin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $enchereur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrixImmediat(): ?int
    {
        return $this->prixImmediat;
    }

    public function setPrixImmediat(?int $prixImmediat): self
    {
        $this->prixImmediat = $prixImmediat;

        return $this;
    }

    public function getPrixDepart(): ?int
    {
        return $this->prixDepart;
    }

    public function setPrixDepart(?int $prixDepart): self
    {
        $this->prixDepart = $prixDepart;

        return $this;
    }

    public function getBid(): ?int
    {
        return $this->bid;
    }

    public function setBid(?int $bid): self
    {
        $this->bid = $bid;

        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(?\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getEnchereur(): ?int
    {
        return $this->enchereur;
    }

    public function setEnchereur(?int $enchereur): self
    {
        $this->enchereur = $enchereur;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}

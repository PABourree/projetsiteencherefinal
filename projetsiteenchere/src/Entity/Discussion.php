<?php

namespace App\Entity;

use App\Repository\DiscussionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiscussionRepository::class)
 */
class Discussion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="discussions")
     */
    private $vendeurid;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="discussions")
     */
    private $acheteurid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVendeurid(): ?user
    {
        return $this->vendeurid;
    }

    public function setVendeurid(?user $vendeurid): self
    {
        $this->vendeurid = $vendeurid;

        return $this;
    }

    public function getAcheteurid(): ?user
    {
        return $this->acheteurid;
    }

    public function setAcheteurid(?user $acheteurid): self
    {
        $this->acheteurid = $acheteurid;

        return $this;
    }
}

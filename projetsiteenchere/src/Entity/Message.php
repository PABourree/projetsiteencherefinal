<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $msgcontenu;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datemsg;

    /**
     * @ORM\ManyToOne(targetEntity=Annonce::class, inversedBy="messages")
     */
    private $annoncemsg;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $msgauteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMsgcontenu(): ?string
    {
        return $this->msgcontenu;
    }

    public function setMsgcontenu(?string $msgcontenu): self
    {
        $this->msgcontenu = $msgcontenu;

        return $this;
    }

    public function getDatemsg(): ?\DateTimeInterface
    {
        return $this->datemsg;
    }

    public function setDatemsg(?\DateTimeInterface $datemsg): self
    {
        $this->datemsg = $datemsg;

        return $this;
    }

    public function getAnnoncemsg(): ?annonce
    {
        return $this->annoncemsg;
    }

    public function setAnnoncemsg(?annonce $annoncemsg): self
    {
        $this->annoncemsg = $annoncemsg;

        return $this;
    }

    public function getMsgauteur(): ?string
    {
        return $this->msgauteur;
    }

    public function setMsgauteur(?string $msgauteur): self
    {
        $this->msgauteur = $msgauteur;

        return $this;
    }
}

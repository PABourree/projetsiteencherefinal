<?php

namespace App\Entity;

use App\Repository\ConversationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConversationRepository::class)
 */
class Conversation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vendeur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $acheteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVendeur(): ?int
    {
        return $this->vendeur;
    }

    public function setVendeur(?int $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getAcheteur(): ?int
    {
        return $this->acheteur;
    }

    public function setAcheteur(?int $acheteur): self
    {
        $this->acheteur = $acheteur;

        return $this;
    }
}

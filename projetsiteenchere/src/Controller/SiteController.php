<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Enchere;
use App\Entity\Message;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SiteController extends AbstractController
{
/**
 * @Route("/site/msg", name="site_msg")
 */
public function msg(Request $request, EntityManagerInterface  $manager){

$repo = $this->getDoctrine()->getRepository(Message::class);
$messages = $repo->findAll();

$id = $_GET['id'];

$repo = $this->getDoctrine()->getRepository(Annonce::class);
$annonce = $repo->find($id);

$annonceAuteur = $annonce->getAuteur();
$userid = $this->getUser()->getId();

if($annonceAuteur->getId() == $userid){
    $auteurmsg = "vendeur";
} else {
    $auteurmsg = "acheteur";
}

dump($auteurmsg);
dump($annonceAuteur);
dump($annonce);
dump($userid);
dump($id);

$newMessage = new Message();

$form = $this->createFormBuilder($newMessage)
             ->add('msgcontenu')
             ->add('save', SubmitType::class, [
                'label' => 'Suivant'
            ])
            ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted()){
                $newMessage->setMsgauteur($auteurmsg);
                $newMessage->setAnnoncemsg($annonce);
                $newMessage->setdatemsg(new \DateTime());
                $manager->persist($newMessage);
                $manager->flush();
            }


return $this->render('site/msg.html.twig',[
    'formMsg' => $form->createView(),
    'messages' => $messages,
]);

}
/**
 * @Route("/site/message", name="site_message")
 */

public function message(){

    $repo = $this->getDoctrine()->getRepository(Annonce::class);
    $annonces = $repo->findAll();
    
    $repo = $this->getDoctrine()->getRepository(Enchere::class);
    $encheres = $repo->findAll();

    $user = $this->getUser()->getId();

    dump($user);
    dump($encheres);
    dump($annonces);
    return $this->render('site/message.html.twig',[
        'annonces' => $annonces,
        'encheres' => $encheres,
        'user' => $user,
    ]);
}   

/**
 * @Route("/site/bidenchere{id}", name="site_bidenchere")
 */
public function bidenchere($id,Request $request){

    $repo = $this->getDoctrine()->getRepository(Annonce::class);
    $annonce = $repo->find($id);

    $repo = $this->getDoctrine()->getRepository(Enchere::class);
    $enchere = $repo->findby(['annonce' => $id]);

    $user = $this->getUser()->getId();
    $data = array(
        'montant'  => ""
    );
    $enchId = $enchere[0];
    $bidform = array();
 
    $form = $this->createFormBuilder($bidform)
                 ->add('montant')
                 ->add('save', SubmitType::class, [
                    'label' => 'Suivant'])
                 ->getForm();
                 

    $form->handleRequest($request);

    dump($enchere[0]);

    if ($form->isSubmitted()){
        $data = $form->getData();
        $em = $this->getDoctrine()->getManager();
        $enchere = $em->getRepository(Enchere::class)->find($enchId);
        $enchere->setprixDepart($data['montant']);
        $enchere->setenchereur($user);
        $em->flush();

    }

    dump($annonce);
    dump($data);
    dump($id);
    dump($user);

    return $this->render('site/bidenchere.html.twig', [
        'controller_name' => 'SiteController',
        'data' => $data,
        'formBid' => $form->createView()
    ]);
}  
    /**
     * @Route("/site", name="site")
     */
    public function index(Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findAll();
        
        $repo = $this->getDoctrine()->getRepository(Enchere::class);
        $encheres = $repo->findAll();

        $data = array(
            'recherche'  => ""
        );
        $recherche = array();
 
        $form = $this->createFormBuilder($recherche)
                     ->add('recherche')
                     ->add('save', SubmitType::class, [
                        'label' => 'Suivant'])
                     ->getForm();
                     

        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $data = $form->getData();
            dump($data);
        }

        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
            'annonces' => $annonces,
            'encheres' => $encheres,
            'data' => $data,
            'formRecherche' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/site/tableau", name="site_tableau")
     */
    public function tableau() {
        
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findAll();
        
        $repo = $this->getDoctrine()->getRepository(Enchere::class);
        $encheres = $repo->findAll();

        $user = $this->getUser();

        dump($annonces);
        dump($encheres);

        return $this->render('site/tableau.html.twig', [
            'controller_name' => 'SiteController',
            'annonces' => $annonces,
            'encheres' => $encheres,
            'user' => $user
        ]);

    }

    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/site/new", name="site_create")
     */
    public function create(Request $request, EntityManagerInterface  $manager){
        $annonce = new Annonce();

        $user = $this->getUser();
        dump($user);

        $form = $this->createFormBuilder($annonce)
                     ->add('title')
                     ->add('content')
                     ->add('categorie', ChoiceType::class, [
                         'choices' => [
                             'Mobilier' => 'mobilier',
                             'Véhicule' => 'vehicule',
                             'Immobilier' => 'immobilier',
                         ],
                     ])
                     ->add('image')
                     ->add('save', SubmitType::class, [
                         'label' => 'Suivant'
                     ])
                     ->getForm();

                     $form->handleRequest($request);

                     if($form->isSubmitted() && $form->isValid()){
                         $annonce->setCreatedAt(new \DateTime());
                         $annonce->setAuteur($user);
                         $manager->persist($annonce);
                         $manager->flush();
                     return $this->redirectToRoute('site_createenchere', [
                            'id' => $annonce->getId()
                        ]);
                     }
            return $this->render('site/create.html.twig',[
                'formAnnonce' => $form->createView()
            ]);
    }
    /**
     * @Route("/site/newenchere{id}", name="site_createenchere")
     */
    public function createenchere($id, Request $request, EntityManagerInterface  $manager){
        
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonce = $repo->find($id);
        
        $enchere = new Enchere();


        $form = $this->createFormBuilder($enchere)
                     ->add('prixImmediat')
                     ->add('prixDepart')
                     ->add('datefin')
                     ->add('save', SubmitType::class, [
                        'label' => 'Suivant'
                    ])
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $enchere->setAnnonce($annonce);
            $manager->persist($enchere);
            $manager->flush();
            return $this->redirectToRoute('site');
        }


        return $this->render('site/createenchere.html.twig', [
            'formEnchere' => $form->createView()
        ]);
    }

    /**
     * @Route("/site/{id}", name="site_show")
     */
    public function show($id, Request $request){
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonce = $repo->find($id);

        $repo = $this->getDoctrine()->getRepository(Enchere::class);
        $encheres = $repo->findAll();

        $user = $this->getUser();

        $data = array(
            'Moyen_de_paiement'  => ""
        );

        $formnb1 = array();

        $form = $this->createFormBuilder($formnb1)
                ->add('Moyen_de_paiement', ChoiceType::class, [
                        'choices' => [
                        'AchatDirect' => 'achatDirect',
                        'Encherir' => 'enchere',
                        ],])
                 ->add('encherir', SubmitType::class, [
                    'label' => 'Suivant'])
                 ->getForm();

        $form->handleRequest($request);
        dump($form);

        if($form->isSubmitted()){
            $data = $form->getData();
            dump($data);
            if ($data['Moyen_de_paiement'] == "enchere" ){
                return $this->redirectToRoute('site_bidenchere',[
                    'id' => $annonce->getId(),  
                ]);
            }
            if ($data['Moyen_de_paiement'] == "achatDirect" ){
                $repo = $this->getDoctrine()->getRepository(Enchere::class);
                $enchere = $repo->findby(['annonce' => $id]);

                $enchId = $enchere[0];
                $user = $this->getUser()->getId();

                $em = $this->getDoctrine()->getManager();
                $enchere = $em->getRepository(Enchere::class)->find($enchId);
                $enchere->setetat('terminee');
                $enchere->setenchereur($user);
                $em->flush();
                return $this->redirectToRoute('site_achatdirect',[
                    'id' => $annonce->getId(),  
                ]);
            }
        }
        

        dump($annonce);
        dump($encheres);

        return $this->render('site/show.html.twig',[
            'annonce' => $annonce,
            'encheres' => $encheres,
            'user' => $user,
            'formnb1' => $form->createView()
        ]);
    }
/**
 * @Route("/site/achatdirect{id}", name="site_achatdirect")
 */
public function achatdirect(){

    return $this->render('site/achatdirect.html.twig');
}   
}

